<?php

namespace App\Http\Controllers\auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index () {


        return view('_auth.register');
    }

    public function store (Request $request) {

        $this->validate($request, [
            'username'  =>"required|max:255",
            'firstname' =>"required|max:255",
            'lastname'  =>"required|max:255",
            'email'     => "required|email|max:255",
            'password'  => "required|confirmed",

        ]);

        User::create([
            'username'  => $request->username,
            'firstname'  => $request->firstname,
            'lastname'  => $request->lastname,
            'email'     => $request->email,
            'password'  => Hash::make($request->password)
        ]);

        auth()->attempt($request->only('email', 'password'));

        return redirect()->route("dashboard");
    }
}
