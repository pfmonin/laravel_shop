
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="#">LaraShop🍀</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
  
        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav me-auto">
                <li class="nav-item">
                    <a class="nav-link active" href="#">Home
                    <span class="visually-hidden">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Features</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Pricing</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                </li>            
            </ul>
            <div class="_auth ml-auto d-flex">
                @auth
                    <div class="d-flex">
                        <a href="{{ route('dashboard') }}" class="btn btn-dark">{{ auth()->user()->username }}</a> 
                        
                        {{-- to avoid cross request bot hacking the logout element need to be in a form and the link must be a button instead of a a href--}}
                        <form action="{{ route("logout") }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-dark" style="margin: 0 2rem">Logout</button> 
                        </form>
                    </div>
                @endauth

                @guest
                <a href="{{ route('register') }}" class="btn btn-dark">Register</a>                    
                <a href="{{ route('login') }}" class="btn btn-dark" style="margin: 0 2rem">Login</a>                    
                @endguest
                <a href="http://127.0.0.1:8000/cart" class="btn btn-dark "><i class="bi bi-cart4"></i>Cart</a>
            </div>
            
        </div>
    </div>
</nav>
  
  