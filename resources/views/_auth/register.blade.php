@extends('layouts.default')

@section('content')



<div id="register" style="width: 35%; margin:3rem auto;">
    
    <h1 class="mb-3">S'enregister</h1>
    <form action="{{route('register')}}" method="POST">
        @csrf
        <div class="mb-3">
            <input name="username" type="text" class="form-control" id="username" placeholder="Username" value="{{old('username')}}">
           
        </div>
        <div class="mb-3">
            <input name="firstname" type="text" class="form-control" id="firstname" placeholder="Firstname" value="{{old('firstname')}}">
        </div>
        <div class="mb-3">
            <input name="lastname" type="text" class="form-control" id="lastname" placeholder="Lastname" value="{{old('lastname')}}">
        </div>

        <div class="mb-3">
            <input name="email" type="email" class="form-control" id="email" placeholder="Email" value="{{old('email')}}">
        </div>

        <div class="mb-3">
            <input name="password" type="password" class="form-control" id="password" placeholder="Password">
        </div>

        <div class="mb-3">
            <input name="password_confirmation" type="password" class="form-control" id="password_confirmation" placeholder="Confirm password">
        </div>
       

        <button type="submit" class="btn btn-primary">Register</button>

    </form>
</div>

@endSection


