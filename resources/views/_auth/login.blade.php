@extends('layouts.default')

@section('content')



<div id="register" style="width: 35%; margin:3rem auto;">
    
    <h1 class="mb-3">Connexion</h1>
    @if (session('status'))
        {{session('status')}}        
    @endif
    <form action="{{route('login')}}" method="POST">        
        @csrf
        <div class="mb-3">
            <input name="email" type="email" class="form-control" id="email" placeholder="Email" value="{{old('email')}}">
        </div>

        <div class="mb-3">
            <input name="password" type="password" class="form-control" id="password" placeholder="Password">
        </div>

        <div class="mb-3 form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1" name="remember">
            <label class="form-check-label" for="exampleCheck1">Check me out</label>
          </div>

        <button type="submit" class="btn btn-primary">Login</button>

    </form>
</div>

@endSection
